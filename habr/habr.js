import { Before, Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

describe('Просмотр новостей на Хабр', () => {
  Given('я на главной странице Хабр', () => {
    cy.visit('https://habr.com/ru/');
  });

  describe('Просмотр всех постов', () => {
    When(/^я нажимаю "Все подряд"$/, () => {
      cy.get('a[href="https://habr.com/ru/all/"]').click();
    });

    Then(/^я вижу все посты$/, () => {
      cy.get('a[class="toggle-menu__item-link toggle-menu__item-link_active"]')
      .contains('Все подряд');
    });
  });

  describe('Простой поиск', () => {
    When(/^я нажимаю на "лупу"$/, query => {
      cy.get('button[title="Поиск по сайту"]').click();
    });

    When(/^я набира в поиске "cypress"$/, query => {
      cy.get('input[type="text"]').type('cypress');
    });

    When(/^я нажимаю на "лупу"$/, () => {
      cy.get('button[title="Поиск по сайту"]').click();
    });

    Then(/^я вижу результаты$/, () => {
      cy.get('h2[class="post__title"]')
      .its('length')
        .then(length => {
          expect(length).to.be.greaterThan(1);

        });
    });
  });

  describe('Смена языка', () => {
    When(/^я нажимаю на иконку смены языка$/, query => {
      cy.get('button[class="btn btn_medium btn_navbar_lang js-show_lang_settings"]').click();
    });

    When(/^я нажимаю "English"$/, query => {
      cy.get('input[id="hl_langs_en"]').click({force: true});
    });

    When(/^я нажимаю на "Сохранить настройки"$/, () => {
      cy.get('button[class="btn btn_blue btn_huge btn_full-width js-popup_save_btn"]').click();
    });

    Then(/^язык страницы изменился$/, () => {
      cy.get('html[lang]').contains('en')
    });
  });
});
