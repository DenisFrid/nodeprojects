# language: ru

Функционал: Просмотр новостей на Хабр

  Предыстория:
    Допустим я на главной странице Хабр

  Сценарий: Просмотр всех постов
    Когда я нажимаю "Все подряд"
    Тогда я вижу все посты

  Сценарий: Простой поиск
    Когда я нажимаю на "лупу"
    И я набира в поиске "cypress"
    И я нажимаю на "лупу"
    Тогда я вижу результаты

  Сценарий: Смена языка
    Когда я нажимаю на иконку смены языка
    И я нажимаю "English"
    И я нажимаю на "Сохранить настройки"
    Тогда язык страницы изменился

